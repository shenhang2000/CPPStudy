﻿// Lesson16.cpp : 此文件包含 "main" 函数。程序执行将在此处开始并结束。
//

#include "pch.h"
#include <iostream>
#include "SpreadSheet.h"
#include "SpreadSheetCell.h"
#include "SpreadSheetApp.h"

int main()
{
	SpreadSheetApp app;

	std::cout << app.GetDefaultSavePath() << std::endl;

	//    std::cout<<SpreadSheet::sDefaultExtension<<std::endl;
//    SpreadSheet::SetDefaultExtension(".xls");
//    std::cout<<SpreadSheet::sDefaultExtension<<std::endl;
	//SpreadSheet ss(4, 5);

	//ss.SetCell(1, 2, "abc");

	////DumpSS(ss);

	//SpreadSheetCell& cell = ss.GetCell(1, 2);

	//cell.SetValue(3);

	//cell.ResetAccessCount(1, 4);

	//SpreadSheetCell cell("2019");

	//SpreadSheetCell cell2(2019);
	//std::string str = "1000";
	//std::string_view sv(str);
	//SpreadSheetCell cell3((std::string_view(str)));

	////SpreadSheetCell cell4=std::string_view("2019") + cell3 ;

	//cell3 += std::string("9999");

	//std::cout << cell3.GetValue() << std::endl;
	return 0;
}

// 运行程序: Ctrl + F5 或调试 >“开始执行(不调试)”菜单
// 调试程序: F5 或调试 >“开始调试”菜单

// 入门提示: 
//   1. 使用解决方案资源管理器窗口添加/管理文件
//   2. 使用团队资源管理器窗口连接到源代码管理
//   3. 使用输出窗口查看生成输出和其他消息
//   4. 使用错误列表窗口查看错误
//   5. 转到“项目”>“添加新项”以创建新的代码文件，或转到“项目”>“添加现有项”以将现有代码文件添加到项目
//   6. 将来，若要再次打开此项目，请转到“文件”>“打开”>“项目”并选择 .sln 文件
