#pragma once

#include <string>
#include "SpreadSheet.h"
#include <string_view>


class SpreadSheetCell
{
public:
	SpreadSheetCell();

	/*explicit*/ SpreadSheetCell(const std::string& strValue);

	/*explicit*/ SpreadSheetCell(std::string_view svValue);

	/*explicit*/ SpreadSheetCell(double dValue);

	SpreadSheetCell(const SpreadSheetCell& other) = default;
	SpreadSheetCell& operator = (const SpreadSheetCell& other) = default;

	~SpreadSheetCell();

	void SetValue(const std::string& strValue);
    void SetValue(int nValue);
    void SetValue(double dValue)=delete ;
    //void SetValue(float dValue)=delete ;

	std::string GetValue() const;
    std::string GetValue();

    void ResetAccessCount(int x,int nAccessCount=0);

    inline int GetAccessCount() const;

    //SpreadSheetCell add(SpreadSheetCell& other);

    //SpreadSheetCell operator+ (const SpreadSheetCell& other);

	SpreadSheetCell& operator += (const SpreadSheetCell& rhs);

	bool operator == (const SpreadSheetCell& rhs);
private:
	std::string mValue;

    /*mutable*/ int mAccessCount=0;

	//friend class SpreadSheet;
	//friend void SpreadSheet::SetCell(int, int, const std::string&);
	//friend std::string SpreadSheet::GetCell(int, int);

	friend void DumpCell(const SpreadSheetCell& other);

public:
    enum class CellType:int
    {
        eCTNone=0,
        eCTInt=1,
        eCTDouble=2,
        eCTStr=3,
        eCTObj=4,
    };
    typedef class CellImpl
    {
    public:
        CellImpl()=default;
        void SetType(CellType nType);
    public:
        CellType mCellType;
    } cell_impl_type;

    struct CellInterval
    {
        int value;
        int type;
    };
};


SpreadSheetCell operator + (const SpreadSheetCell& lhs, const SpreadSheetCell& rhs);

