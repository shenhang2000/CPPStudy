#pragma once

class IntArrayList
{
public:
	typedef int value_type;
	IntArrayList(size_t i=30)
		:mLen(i)
	{
		if (mLen > 0)
		{
			mPInt = new value_type[mLen];
		}
		else
		{
			mPInt = nullptr;
		}
	}
	~IntArrayList()
	{
		delete[] mPInt;
		mPInt = nullptr;
	}
	void resize(size_t nLen)
	{
		delete[] mPInt;
		//参数检查...
		mLen = nLen;
		mPInt = new value_type[nLen];
		for (size_t i = 0; i < mLen; i++)
		{
			mPInt[i] = 1;
		}
	}

	value_type& operator [](size_t i)
	{
		//参数检查...
		return mPInt[i];
	}
	size_t size()
	{
		return mLen;
	}
private:
	value_type* mPInt;
	size_t mLen;
};

