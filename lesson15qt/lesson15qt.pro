TEMPLATE = app
CONFIG += console c++17
CONFIG -= app_bundle
CONFIG -= qt

SOURCES += \
        main.cpp \
    SpreadSheet.cpp \
    SpreadSheetCell.cpp \
    SpreadSheetApp.cpp

HEADERS += \
    SpreadSheet.h \
    SpreadSheetCell.h \
    SpreadSheetApp.h
