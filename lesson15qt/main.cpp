#include <iostream>
using namespace std;

#include "SpreadSheet.h"
#include "SpreadSheetCell.h"
#include "SpreadSheetApp.h"

void DumpSS(const SpreadSheet& ss)
{
    const SpreadSheetCell& cell=ss.GetCell(1,2);
    std::cout<<cell.GetValue()<<std::endl;
}

int main()
{
    SpreadSheetApp app;
//    std::cout<<SpreadSheet::sDefaultExtension<<std::endl;
//    SpreadSheet::SetDefaultExtension(".xls");
//    std::cout<<SpreadSheet::sDefaultExtension<<std::endl;
    SpreadSheet ss(4,5,&app);

    SpreadSheet ss2(40,50,&app);

    ss.SetCell(1,2,"hello");

    ss.SetCell(2,3,",world");

    SpreadSheetCell ssc = ss.GetCell(1,2) + ss.GetCell(2,3);

    //DumpSS(ss);

    SpreadSheetCell& cell=ss.GetCell(1,2);

    cell.SetValue(3);

    cell.ResetAccessCount(1,4);

    std::cout<<ssc.GetValue()<<std::endl;

    //SpreadSheetCell::CellImpl impl;
    //impl.SetType(SpreadSheetCell::CellType::eCTInt);



    return 0;
}
