﻿// Lesson26_3.cpp : 此文件包含 "main" 函数。程序执行将在此处开始并结束。
//

#include "pch.h"
#include <iostream>
#include <tuple>
#include <array>
#include "Row.h"
#include "VariadicTemplate.h"

int main()
{
	//Row<Row<int> > row;
	//row[3][3] = 3;
	//std::cout << row[3][3]<<std::endl; 

	Row<int, 3> row3d;//-->Row<int,2>-->Row<int,1>

	row3d[3][3][3]=2009;
	std::cout << row3d[3][3][3] << std::endl;

	std::array<std::array<int,5>, 3> tarray;

	Row<int, 2> row2d;
	std::cout << row2d[2][2] << std::endl;

	Row<int, 30> row30d;

	//std::vector<std::vector<std::vector<std::vector<...>>>>>>....

	//std::vector<std::vector<std::vector<int>> > vec3d;

	//MyTemplate<int,int,int> IntTemplate;
	//MyTemplate<int, int> IntTemplate;
	////MyTemplate<double, double> IntDoubleTemplate;

	//func(1, 2, 3);
	//func(1, 2);

	MyCPP11VariadicTemplate<int> intvat;
	MyCPP11VariadicTemplate<int, int> intintvat;
	MyCPP11VariadicTemplate<> voidvat;

	TestVariadicTempls<int> voidvt;


	VariadicFunc(3,"str",2.5,voidvt,voidvat);
	return 0;
}

// 运行程序: Ctrl + F5 或调试 >“开始执行(不调试)”菜单
// 调试程序: F5 或调试 >“开始调试”菜单

// 入门提示: 
//   1. 使用解决方案资源管理器窗口添加/管理文件
//   2. 使用团队资源管理器窗口连接到源代码管理
//   3. 使用输出窗口查看生成输出和其他消息
//   4. 使用错误列表窗口查看错误
//   5. 转到“项目”>“添加新项”以创建新的代码文件，或转到“项目”>“添加现有项”以将现有代码文件添加到项目
//   6. 将来，若要再次打开此项目，请转到“文件”>“打开”>“项目”并选择 .sln 文件
