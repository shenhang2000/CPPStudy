#pragma once

template<typename T1=void, typename T2=void, typename T3=void>
class MyTemplate
{

};

template<typename T1, typename T2>
class MyTemplate<T1,T2,void>
{

};

template<typename T1,typename T2,typename T3>
void func(T1 arg1, T2 arg2, T3 arg3)
{

}

template<typename T1, typename T2>
void func(T1 arg1, T2 arg2)
{

}

//printf("%s", "abc");

template <typename ... Args>
class MyCPP11VariadicTemplate
{

};


template<typename T,typename ...Args>
class TestVariadicTempls
{

};

template<typename ... ArgTypes>
void VariadicFuncCallee(ArgTypes ... args)
{

}

template<typename T, typename ... ArgTypes>
void VariadicFunc(T t, ArgTypes ... args)
{
	VariadicFuncCallee(args ...);
}






