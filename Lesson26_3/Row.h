#pragma once
#include <vector>
template<typename T,size_t N>
class Row 
{
public:
	explicit Row(size_t nInitSize = 30)
	{
		resize(nInitSize);
	}
	Row<T,N-1>& operator [](size_t nIndex)
	{
		return mElements[nIndex];
	}
	const Row<T, N - 1>& operator [](size_t nIndex) const
	{
		return mElements[nIndex];
	}

	void resize(size_t newSize)
	{
		mElements.resize(newSize);
		for (size_t i = 0; i < mElements.size(); i++)
		{
			mElements.resize(newSize);
		}
	}

	size_t size() const
	{
		return mElements.size();
	}

protected:
	std::vector<Row<T,N-1> > mElements;
};


template<typename T>
class Row<T,1>
{
public:
	explicit Row(size_t nInitSize = 30)
	{
		resize(nInitSize);
	}
	T& operator [](size_t nIndex)
	{
		return mElements[nIndex];
	}
	const T& operator [](size_t nIndex) const
	{
		return mElements[nIndex];
	}

	void resize(size_t newSize)
	{
		mElements.resize(newSize);
	}

protected:
	std::vector<T > mElements;
};
