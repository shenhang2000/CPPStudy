#pragma once

#include "SpreadSheetApp.h"

class SpreadSheetApp::Impl
{
public:
	Impl();

	Impl(const Impl& rhs);

	Impl& operator=(const Impl& rhs);

	std::string GetDefaultSavePath() const;

private:
	bool IsMobile() const;

	void swap(Impl& first) noexcept;
private:
	bool isCreated = false;
	int* mPtr;
};