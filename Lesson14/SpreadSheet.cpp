#include "pch.h"
#include "SpreadSheet.h"
#include "SpreadSheetCell.h"
#include <iostream>


SpreadSheet::SpreadSheet()
	:mCells(nullptr), mRow(0), mColumn(0)
{
	std::cout << "default construct" << std::endl;
}

SpreadSheet::SpreadSheet(int nRow, int nColumn)
	:mCells(nullptr), mRow(nRow),mColumn(nColumn)
{
	std::cout << "construct" << std::endl;
	Init();
}

SpreadSheet::SpreadSheet(const SpreadSheet & other)
{
	std::cout << "copy construct" << std::endl;
	InitFrom(other);
}


SpreadSheet& SpreadSheet::operator=(const SpreadSheet& other)
{
	std::cout << "copy operator=..." << std::endl;
	if (this==&other)
	{
		return *this;
	}

	if (other.mRow == mRow && other.mColumn == mColumn)
	{
		for (int r = 0; r < mRow; ++r)
		{
			for (int c = 0; c < mColumn; ++c)
			{
				mCells[r][c] = other.mCells[r][c];
			}
		}
		return *this;
	}
	else
	{
		//Fini();
		//�������ڴ�
		//InitFrom(other);
		SpreadSheet temp(other);
		swap(*this, temp);
		return *this;
	}
}


void SpreadSheet::MoveFrom(SpreadSheet & other)
{
	mName = std::move(other.mName);

	mRow = other.mRow;
	mColumn = other.mColumn;
	mCells = other.mCells;

	other.mColumn = 0;
	other.mRow = 0;
	other.mCells = nullptr;
}

SpreadSheet::SpreadSheet(SpreadSheet && other) noexcept
	:SpreadSheet()
{
	std::cout << "move construct" << std::endl;
	//MoveFrom(other);
	swap(*this, other);
}

SpreadSheet & SpreadSheet::operator=(SpreadSheet && other) noexcept
{
	//std::cout << "move operator=..." << std::endl;
	//if (this == &other)
	//{
	//	return *this;
	//}
	//Fini();

	//MoveFrom(other);

	SpreadSheet temp(std::move(other));
	swap(*this,temp);
	return *this;
}


SpreadSheet::~SpreadSheet()
{
	std::cout << "destruct..." << std::endl;
	Fini();
}

bool SpreadSheet::SetCell(int nRow, int nColumn, const std::string & mValue)
{
	int nMyRow = nRow - 1;
	if (nMyRow >= mRow || nMyRow < 0)
	{
		return false;
	}
	int nMyColumn = nColumn - 1;
	if (nMyColumn >= mColumn || nMyColumn < 0)
	{
		return false;
	}
	mCells[nMyRow][nMyColumn].SetValue(mValue);
	return true;
}

bool SpreadSheet::GetCell(int nRow, int nColumn, std::string& strOutValue) const
{
	int nMyRow = nRow - 1;
	if (nMyRow >= mRow || nMyRow < 0)
	{
		return false;
	}
	int nMyColumn = nColumn - 1;
	if (nMyColumn >= mColumn || nMyColumn < 0)
	{
		return false;
	}
	strOutValue = mCells[nMyRow][nMyColumn].GetValue();
	return true;
}

void SpreadSheet::Init()
{
	mCells = new SpreadSheetCell*[mRow];
	for (int r = 0; r < mRow; ++r)
	{
		mCells[r] = new SpreadSheetCell[mColumn];
	}
}

void SpreadSheet::Fini()
{
	//�ͷž��ڴ�
	if (nullptr != mCells)
	{
		//std::cout << "free mem:" << mCells << std::endl;
		for (int i = 0; i < mRow; i++)
		{
			delete[] mCells[i];
		}
		delete[] mCells;
		mCells = nullptr;
	}
}

void SpreadSheet::InitFrom(const SpreadSheet & other)
{
	mRow = other.mRow;
	mColumn = other.mColumn;
	mCells = new SpreadSheetCell*[mRow];
	for (int r = 0; r < mRow; ++r)
	{
		mCells[r] = new SpreadSheetCell[mColumn];
		for (int c = 0; c < mColumn; ++c)
		{
			mCells[r][c] = other.mCells[r][c];
		}
	}
}



void DumpCell(const SpreadSheetCell& other)
{
	std::cout << other.mValue << std::endl;
}

void swap(SpreadSheet & first, SpreadSheet & second) noexcept
{
	using std::swap;
	std::swap(first.mName, second.mName);
	std::swap(first.mRow, second.mRow);
	std::swap(first.mColumn, second.mColumn);
	std::swap(first.mCells, second.mCells);
}
