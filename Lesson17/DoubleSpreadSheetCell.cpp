#include "pch.h"
#include "DoubleSpreadSheetCell.h"
#include <iostream>


DoubleSpreadSheetCell::DoubleSpreadSheetCell()
	:mdValue(0.0)
{
}


DoubleSpreadSheetCell::~DoubleSpreadSheetCell()
{
}


void DoubleSpreadSheetCell::SetValue(const std::string & strValue)
{
	mdValue = std::stod(strValue);
}

void DoubleSpreadSheetCell::SetValue(double dValue)
{

}

std::string DoubleSpreadSheetCell::Trim(const std::string & strValue)
{
	std::string strTimmedValue = SpreadSheetCell::Trim(strValue);
	if (strTimmedValue.find(" ") != std::string::npos)
	{
		return "";
	}
	return strTimmedValue;
}

std::string DoubleSpreadSheetCell::GetValue() const
{
	return std::to_string(mdValue);
}

std::string DoubleSpreadSheetCell::GetValue()
{
	return std::to_string(mdValue);
}


//void DoubleSpreadSheetCell::SetValue(double dValue)
//{
//	mdValue = dValue;
//}
//
//double DoubleSpreadSheetCell::GetValue()
//{
//	return mdValue;
//}
