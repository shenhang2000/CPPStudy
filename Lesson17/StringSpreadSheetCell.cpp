#include "pch.h"
#include "StringSpreadSheetCell.h"
#include <iostream>

StringSpreadSheetCell::StringSpreadSheetCell()
{
}


StringSpreadSheetCell::~StringSpreadSheetCell()
{
	std::cout << "StringSpreadSheetCell dtor" << std::endl;
}

void StringSpreadSheetCell::SetValue(const std::string & strValue)
{
	std::cout << "derived setvalue" << std::endl;
	mValue = strValue;
}

void StringSpreadSheetCell::SetValue(std::string_view strStrView)
{
	std::cout << "setvalue string_view" << std::endl;
	mValue = strStrView;
}

//std::string StringSpreadSheetCell::GetValue() const
//{
//	return mValue;
//}
//
//std::string StringSpreadSheetCell::GetValue()
//{
//	return mValue;
//}
