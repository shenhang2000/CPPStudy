#pragma once

#include <string>
#include "SpreadSheet.h"
#include <string_view>


class SpreadSheetCell
{
public:
	SpreadSheetCell();

	SpreadSheetCell(const SpreadSheetCell& other) = default;
	SpreadSheetCell& operator = (const SpreadSheetCell& other) = default;

	virtual ~SpreadSheetCell();

	virtual void SetValue(const std::string& strValue);
    //void SetValue(float dValue)=delete ;

	virtual std::string GetValue() const;
	virtual std::string GetValue();

    virtual void ResetAccessCount(int x,int nAccessCount=0) final;

    inline int GetAccessCount() const;

    //SpreadSheetCell add(SpreadSheetCell& other);

    //SpreadSheetCell operator+ (const SpreadSheetCell& other);

	//SpreadSheetCell& operator += (const SpreadSheetCell& rhs);

	//bool operator == (const SpreadSheetCell& rhs);

	virtual std::string Trim(const std::string& strValue);
protected:
    /*mutable*/ int mAccessCount=0;

	//friend class SpreadSheet;
	//friend void SpreadSheet::SetCell(int, int, const std::string&);
	//friend std::string SpreadSheet::GetCell(int, int);

	friend void DumpCell(const SpreadSheetCell& other);

public:
    enum class CellType:int
    {
        eCTNone=0,
        eCTInt=1,
        eCTDouble=2,
        eCTStr=3,
        eCTObj=4,
    };
public:
	CellType m_Type;
public:
	CellType GetType();
};


//SpreadSheetCell operator + (const SpreadSheetCell& lhs, const SpreadSheetCell& rhs);

