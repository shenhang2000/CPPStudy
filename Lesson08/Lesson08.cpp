﻿/*
	@author Admin
	@DateTime:20190616
	@Remark:主函数入口文件
	@Update time changer Content
	20190701 Admin    修改了DataLoader，增加了一个有参构造函数
*/

#include "pch.h"
#include <iostream>
#include <vector>
//这是单行注释


/*
这是第一行注释
这是第二行注释


*/


/*
1.声明一个DataLoader对象
	DataLoader dl;
2.打开文件
	bool bOpened=dl.OpenFile("a.txt");
3.读取数据
	if(bOpened)
	{
		std::vector<int> vecData = dl.ReadData();
	}
*/

class DataLoader
{
public:

	DataLoader(const std::string& strFilePath);
	//构造函数，参数是一个文件的绝对路径
	/*
		@param
		@return
		@exception
		@author
		@update 
		@remark
	*/
	bool OpenFile(const std::string& strFilePath);

	std::vector<int> ReadData()
	{
		std::vector<int> res;
		//1.每次读取一行
		
		while(mFile.endoffile)
		{
			std::string line = mFile.ReadLine();
			std::vector<std::string> vecItems;
			//按逗号拆分
			split(line, vecItems, ",");
			if (vecItems == 8) //规定每行的必须是8个数字组合
			{
				for (size_t i = 0; i < vecItems.size(); i++)
				{
					//将每个字符转换成数字
					res.push_back(stoi(vecItems[i]));
				}
			}
		}
		
		return res;
	}
private:
	File mFile;
};


bool ReadData(std::vector<int> res, const std::string& strFilePath)
{
	//1.打开文件
	DataLoader dl;

	//2.读取文件
	dl.OpenFile(strFilePath);

	//2.1.解析内容

	res = dl.ReadData();

	//2.2 校验数据

	//存储
}


T first(T* pStr,int nLen);


first<char>(pStr, 5);

first<double>(pDbl, 5);

int main()
{
	int i = 3;
    std::cout << "Hello World!\n"; 
}

// 运行程序: Ctrl + F5 或调试 >“开始执行(不调试)”菜单
// 调试程序: F5 或调试 >“开始调试”菜单

// 入门提示: 
//   1. 使用解决方案资源管理器窗口添加/管理文件
//   2. 使用团队资源管理器窗口连接到源代码管理
//   3. 使用输出窗口查看生成输出和其他消息
//   4. 使用错误列表窗口查看错误
//   5. 转到“项目”>“添加新项”以创建新的代码文件，或转到“项目”>“添加现有项”以将现有代码文件添加到项目
//   6. 将来，若要再次打开此项目，请转到“文件”>“打开”>“项目”并选择 .sln 文件
