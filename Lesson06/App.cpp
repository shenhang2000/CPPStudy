#include "pch.h"
#include "App.h"
#include <iostream>
#include <string>
#include "Employee.h"

App::App()
{
}


App::~App()
{
}


void App::DisplayMenu()
{
	bool bQuit = false;
	while (!bQuit)
	{
		std::cout << "1.添加员工" << std::endl;
		std::cout << "2.移除员工" << std::endl;
		std::cout << "3.员工升职" << std::endl;
		std::cout << "4.查看所有员工(包括已经解雇的)" << std::endl;
		std::cout << "5.退出" << std::endl;

		int selection;

		std::cin >> selection;
		bool bOpSuccess = false;
		switch (selection)
		{
		case 1:
		{
			//add
			bOpSuccess = Add();
		}
		break;
		case 2:
		{
			//remove
			bOpSuccess = Remove();
		}
		break;
		case 3:
		{
			//promote
			bOpSuccess = Promote();
		}
		break;
		case 4:
		{
			//show all;
			bOpSuccess = PrintAll();
		}
		break;
		case 5:
		{
			//quit
			bQuit = true;
			bOpSuccess = true;
		}
		break;
		default:
		{
			std::cout << "输入出错，请重新输入！！！" << std::endl;
		}
		break;
		}
		if (!bOpSuccess)
		{
			std::cout << "操作失败，请重试！！！" << std::endl;
		}
	}
}

bool App::Add()
{
	std::cout << "请输入名字:";
	std::string strName;
	std::cin >> strName;

	return mDatabase.addEmployee(strName);
}

bool App::Remove()
{
	std::cout << "请输入工号:";
	int nID;
	std::cin >> nID;
	return mDatabase.removeEmployee(nID);
}

bool App::Promote()
{
	std::cout << "请输入工号:";
	int nID;
	std::cin >> nID;

	std::cout << "增加的薪水:";
	int nSalary;
	std::cin >> nSalary;
	try
	{
		Employee& e = mDatabase.getEmployee(nID);
		if (e.GetHired())
		{
			e.SetSalary(e.GetSalary() + nSalary);
			return true;
		}
		return false;
	}
	catch (std::logic_error& e)
	{
		std::cout << "promote failed:" << e.what() << std::endl;
	}
	return false;
}

bool App::PrintAll()
{
	std::cout << "----------------所有员工信息-----------------" << std::endl;
	std::vector<Employee>& allEmployees=mDatabase.GetAllEmployees();
	for (size_t i = 0; i < allEmployees.size(); i++)
	{
		std::cout << allEmployees[i].GetID() << "\t" << 
			allEmployees[i].GetName() << "\t" << allEmployees[i].GetSalary() 
			<<"\t"<< (allEmployees[i].GetHired()?"在职":"离职")
			<< std::endl;
	}
	std::cout << "---------------------------------------------" << std::endl;
	return true;
}
