#pragma once
#include <vector>
#include "Employee.h"

class Database
{
public:
	Database();
	~Database();

	bool addEmployee(const std::string& strName);

	bool removeEmployee(int nID);

	std::vector<Employee>& GetAllEmployees();

	Employee& getEmployee(int nID);
private:
	std::vector<Employee> mEmployees;
	static int kStartEmployeeID;

	static Employee kInvalidEmployee;
};

