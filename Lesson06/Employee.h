#pragma once

#include <string>

class Employee
{
public:
	Employee(const std::string& strName);
	~Employee();

	int GetID();
	void SetID(int nID);

	int GetSalary() const;

	void SetSalary(int nSalary);

	std::string GetName() const;

	void SetHired(bool bHired);
	bool GetHired() const;

	bool IsValid();
	void SetValid(bool bValid);
private:
	int mID;
	int mSalary;
	std::string mName;

	bool mHired;

	static int kDefaultSalary;

	bool mValid;
};

