#include "pch.h"
#include "Database.h"
#include <stdexcept>


Database::Database()
{
	kInvalidEmployee.SetValid(false);
}


Database::~Database()
{
}

bool Database::addEmployee(const std::string & strName)
{
	int nCurID = kStartEmployeeID;
	if (!mEmployees.empty())
	{
		nCurID = mEmployees[mEmployees.size()-1].GetID();
		nCurID++;
	}

	Employee e(strName);
	e.SetID(nCurID);
	e.SetHired(true);

	mEmployees.push_back(e);
	return true;
}

bool Database::removeEmployee(int nID)
{
	for (size_t i = 0; i < mEmployees.size(); i++)
	{
		if (mEmployees[i].GetID() == nID)
		{
			mEmployees[i].SetHired(false);
			return true;
		}
	}
	return false;
}

std::vector<Employee>& Database::GetAllEmployees()
{
	// TODO: 在此处插入 return 语句
	return mEmployees;
}

Employee & Database::getEmployee(int nID)
{
	// TODO: 在此处插入 return 语句
	for (size_t i = 0; i < mEmployees.size(); i++)
	{
		if (mEmployees[i].GetID() == nID)
		{
			return mEmployees[i];
		}
	}
	//1.抛异常
	throw std::logic_error("not found employee");

	//2.返回一个全局的无效值
	//return kInvalidEmployee;
}


int Database::kStartEmployeeID=1001;
Employee Database::kInvalidEmployee("");