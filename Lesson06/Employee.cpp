#include "pch.h"
#include "Employee.h"


Employee::Employee(const std::string& strName)
	:mName(strName), mSalary(kDefaultSalary), mHired(false), mValid(true)
{

}


Employee::~Employee()
{
}

int Employee::GetID()
{
	return mID;
}

void Employee::SetID(int nID)
{
	mID = nID;
}

int Employee::GetSalary() const
{
	return mSalary;
}

void Employee::SetSalary(int nSalary)
{
	mSalary = nSalary;
}

std::string Employee::GetName() const
{
	return mName;
}

void Employee::SetHired(bool bHired)
{
	mHired = bHired;
}

bool Employee::GetHired() const
{
	return mHired;
}

bool Employee::IsValid()
{
	return false;
}

void Employee::SetValid(bool bValid)
{
	mValid = bValid;
}


int Employee::kDefaultSalary = 3000;