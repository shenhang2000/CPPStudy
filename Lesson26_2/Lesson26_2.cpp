﻿// Lesson26_2.cpp : 此文件包含 "main" 函数。程序执行将在此处开始并结束。
//

#include "pch.h"
#include <iostream>
#include <string>
#include "Grid.h"
#include "TemplateFunction.h"

int main()
{
	//FullClass fc;
	//fc.func1();
	Grid<int, 10, 10> grid;
	Grid<std::string, 10, 10> strgrid;

	Grid<int*, 10, 10> pintgrid;

    //std::cout << strgrid.at(0,0).value()<<std::endl;
	std::cout << *pintgrid.at(0, 0).value() << std::endl;


	//int intarr[7] = {1,2,3,4,5,6,7};
	//int valueToFind = 5;

	//size_t iFind = Find(valueToFind, intarr, 7);
	//if (iFind == NOT_FOUND_INDEX)
	//{
	//	std::cout << "not found" << std::endl;
	//}
	//else
	//{
	//	std::cout << "the value in the array index is:" << iFind << std::endl;
	//}

	//int* pArray[7] = {new int(1),new int(2),new int(3),new int(4)};
	//int* valueToFind = new int(2);

	//size_t iFind = Find(valueToFind, pArray, 7);
	//if (iFind == NOT_FOUND_INDEX)
	//{
	//	std::cout << "not found" << std::endl;
	//}
	//else
	//{
	//	std::cout << "the value in the array index is:" << iFind << std::endl;
	//}
	char one[] = {"one"}, two[] = { "two" }, three[] = { "three" };
	char* one_two_thead[] = {one,two,three};
	char sa[] = {"one"};
	const char* words[4] = { "one","two","three","four" };
	const char* strToFind = "three";

	//size_t iFind = Find<char*>(sa, one_two_thead, 4);
	size_t iFind = Find<const char*>(strToFind, words, 4);
	if (iFind == NOT_FOUND_INDEX)
	{
		std::cout << "not found" << std::endl;
	}
	else
	{
		std::cout << "the value in the array index is:" << iFind << std::endl;
	}
}

// 运行程序: Ctrl + F5 或调试 >“开始执行(不调试)”菜单
// 调试程序: F5 或调试 >“开始调试”菜单

// 入门提示: 
//   1. 使用解决方案资源管理器窗口添加/管理文件
//   2. 使用团队资源管理器窗口连接到源代码管理
//   3. 使用输出窗口查看生成输出和其他消息
//   4. 使用错误列表窗口查看错误
//   5. 转到“项目”>“添加新项”以创建新的代码文件，或转到“项目”>“添加现有项”以将现有代码文件添加到项目
//   6. 将来，若要再次打开此项目，请转到“文件”>“打开”>“项目”并选择 .sln 文件
