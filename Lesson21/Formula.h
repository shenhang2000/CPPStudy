#pragma once
#include <string>

class Formula
{
public:
	Formula();
	~Formula();
public:
	void SetFormula(const std::string& strFormula);
	virtual std::string GetValue();
	virtual std::string GetValue() const;
};

