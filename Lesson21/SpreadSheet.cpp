#include "pch.h"
#include "SpreadSheet.h"
#include "SpreadSheetCell.h"
//#include "SpreadSheetApp.h"
#include <iostream>
#include <map>


SpreadSheet::SpreadSheet()
    :mCells(nullptr), mRow(0), mColumn(0),mApp(nullptr)
{
    std::cout << "default construct" << std::endl;
}

SpreadSheet::SpreadSheet(int nRow, int nColumn,SpreadSheetApp* app)
    :mCells(nullptr), mRow(nRow),mColumn(nColumn),mApp(nullptr)
{
	std::cout << "construct" << std::endl;
	Init();
}

SpreadSheet::SpreadSheet(const SpreadSheet & other)
    :mApp(other.mApp)
{
	std::cout << "copy construct" << std::endl;
	InitFrom(other);
}


SpreadSheet& SpreadSheet::operator=(const SpreadSheet& other)
{
	std::cout << "copy operator=..." << std::endl;
	if (this==&other)
	{
		return *this;
	}

	if (other.mRow == mRow && other.mColumn == mColumn)
	{
		for (int r = 0; r < mRow; ++r)
		{
			for (int c = 0; c < mColumn; ++c)
			{
				mCells[r][c] = other.mCells[r][c];
			}
		}
		return *this;
	}
	else
	{
		//Fini();
		//申请新内存
		//InitFrom(other);
		SpreadSheet temp(other);
		swap(*this, temp);
		return *this;
	}
}


void SpreadSheet::MoveFrom(SpreadSheet & other)
{
	mName = std::move(other.mName);

	mRow = other.mRow;
	mColumn = other.mColumn;
	mCells = other.mCells;

	other.mColumn = 0;
	other.mRow = 0;
	other.mCells = nullptr;
}

SpreadSheet::SpreadSheet(SpreadSheet && other) noexcept
    :SpreadSheet()
{

	std::cout << "move construct" << std::endl;
	//MoveFrom(other);
	swap(*this, other);
}

SpreadSheet & SpreadSheet::operator=(SpreadSheet && other) noexcept
{
	//std::cout << "move operator=..." << std::endl;
	//if (this == &other)
	//{
	//	return *this;
	//}
	//Fini();

	//MoveFrom(other);

	SpreadSheet temp(std::move(other));
	swap(*this,temp);
    return *this;
}

void SpreadSheet::Save(const std::string& filePath)
{
    if(sDefaultExtension.empty())
    {
        SetDefaultExtension(".xls");
    }
    std::string fileFullName = filePath + sDefaultExtension;
}

void SpreadSheet::SetDefaultExtension(const std::string &strExt)
{
    sDefaultExtension=strExt;
}

void SpreadSheet::DumpSpreadSheet(SpreadSheet &spreadSheet)
{
    for (int i=0;i<spreadSheet.mRow;++i) {
        for (int j=0;j<spreadSheet.mColumn;++j) {
            std::cout<<spreadSheet.mCells[i][j].GetValue()<<" ";
        }
        std::cout<<std::endl;
    }
}

void SpreadSheet::SetApp(SpreadSheetApp *pApp)
{
    mApp=pApp;
}


SpreadSheet::~SpreadSheet()
{
	std::cout << "destruct..." << std::endl;
	Fini();
}

bool SpreadSheet::SetCell(int nRow, int nColumn, const std::string & mValue)
{
	int nMyRow = nRow - 1;
	if (nMyRow >= mRow || nMyRow < 0)
	{
		return false;
	}
	int nMyColumn = nColumn - 1;
	if (nMyColumn >= mColumn || nMyColumn < 0)
	{
		return false;
	}
	mCells[nMyRow][nMyColumn].SetValue(mValue);
	return true;
}

bool SpreadSheet::GetCell(int nRow, int nColumn, std::string& strOutValue) const
{
	int nMyRow = nRow - 1;
	if (nMyRow >= mRow || nMyRow < 0)
	{
		return false;
	}
	int nMyColumn = nColumn - 1;
	if (nMyColumn >= mColumn || nMyColumn < 0)
	{
		return false;
	}
	strOutValue = mCells[nMyRow][nMyColumn].GetValue();
    return true;
}

SpreadSheetCell &SpreadSheet::GetCell(int nRow, int nColumn)
{
    return const_cast<SpreadSheetCell&>(
                const_cast<const SpreadSheet&>(*this).GetCell(nRow,nColumn)
                );
}

const SpreadSheetCell &SpreadSheet::GetCell(int nRow, int nColumn) const
{
    //todo 越界检查
    int nMyRow = nRow - 1;
    if (nMyRow >= mRow || nMyRow < 0)
    {
        //return false;
        throw std::out_of_range("row out of range");
    }
    int nMyColumn = nColumn - 1;
    if (nMyColumn >= mColumn || nMyColumn < 0)
    {
        //return false;
        throw std::out_of_range("column out of range");
    }
    return mCells[nMyRow][nMyColumn];
}

SpreadSheetCell & SpreadSheet::operator[](Coord c)
{
	return GetCell(c.x,c.y);
}


void SpreadSheet::Init()
{
	mCells = new SpreadSheetCell*[mRow];
	for (int r = 0; r < mRow; ++r)
	{
		mCells[r] = new SpreadSheetCell[mColumn];
	}
}

void SpreadSheet::Fini()
{
	//释放旧内存
	if (nullptr != mCells)
	{
		//std::cout << "free mem:" << mCells << std::endl;
		for (int i = 0; i < mRow; i++)
		{
			delete[] mCells[i];
		}
		delete[] mCells;
		mCells = nullptr;
	}
}

void SpreadSheet::InitFrom(const SpreadSheet & other)
{
	mRow = other.mRow;
	mColumn = other.mColumn;
	mCells = new SpreadSheetCell*[mRow];
	for (int r = 0; r < mRow; ++r)
	{
		mCells[r] = new SpreadSheetCell[mColumn];
		for (int c = 0; c < mColumn; ++c)
		{
			mCells[r][c] = other.mCells[r][c];
		}
	}
}



void DumpCell(const SpreadSheetCell& other)
{
	std::cout << other.GetValue() << std::endl;
}

std::ostream & operator<<(std::ostream & ostr, SpreadSheetCell & cell)
{
	// TODO: 在此处插入 return 语句
	std::map< SpreadSheetCell::CellType,std::string> mapCellType2Str=
	{
		{SpreadSheetCell::CellType::eCTNone,"eCTNone"},
		{SpreadSheetCell::CellType::eCTInt,"eCTInt"},
		{SpreadSheetCell::CellType::eCTDouble,"eCTDouble"},
		{SpreadSheetCell::CellType::eCTStr,"eCTStr"},
		{SpreadSheetCell::CellType::eCTObj,"eCTObj"},
	};
	ostr << "[" << mapCellType2Str[cell.GetType()] << "," << cell.GetValue() << "]";
	return ostr;
}

//std::istream & operator>>(std::istream & istr, SpreadSheetCell & cell)
//{
//	 TODO: 在此处插入 return 语句
//	int celltype;
//	istr >> celltype;
//	cell.m_Type = (SpreadSheetCell::CellType)celltype;
//	istr >> mdValue;
//	return istr;
//}

SpreadSheetCell operator+(const SpreadSheetCell & lhs, const SpreadSheetCell & rhs)
{
	std::string str = lhs.GetValue() + rhs.GetValue();
	SpreadSheetCell cell;
	cell.SetValue(str);
	return cell;
}

void swap(SpreadSheet & first, SpreadSheet & second) noexcept
{
	using std::swap;
    std::swap(first.mApp,second.mApp);
	std::swap(first.mName, second.mName);
	std::swap(first.mRow, second.mRow);
	std::swap(first.mColumn, second.mColumn);
	std::swap(first.mCells, second.mCells);
}


std::string SpreadSheet::sDefaultExtension=".xls";

std::string SpreadSheet::sDefaultFileName="untitled spreadsheet ";

