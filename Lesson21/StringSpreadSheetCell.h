#pragma once
#include "SpreadSheetCell.h"
#include "Formula.h"

class StringSpreadSheetCell :
	public SpreadSheetCell,public Formula
{
public:
	StringSpreadSheetCell();
	~StringSpreadSheetCell();
public:
	virtual void SetValue(const std::string& strValue) override;
	//新增接口，支持将double类型的数据传入
	virtual void SetValue(std::string_view strStrView);
	
	//std::string GetValue() const override;
	//std::string GetValue() override;
	using SpreadSheetCell::GetValue;
private:
	std::string mValue;

};

