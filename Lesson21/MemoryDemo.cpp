#include "pch.h"
#include "MemoryDemo.h"
#include <iostream>

MemoryDemo::MemoryDemo()
{
}

void * MemoryDemo::operator new(size_t size,const char* pStr,int x)
{
	std::cout << "operator new"<<pStr << std::endl;
	return ::operator new(size);
}

void * MemoryDemo::operator new[](size_t size)
{
	std::cout << "operator new[]" << std::endl;
	return ::operator new[](size);
}

void * MemoryDemo::operator new(size_t size, const std::nothrow_t &nothrow) noexcept
{
	std::cout << "operator new nothrow" << std::endl;
	return ::operator new(size,nothrow);
}

void * MemoryDemo::operator new[](size_t size, const std::nothrow_t &nothrow) noexcept
{
	std::cout << "operator new[] nothrow" << std::endl;
	return ::operator new[](size, nothrow);
}

void MemoryDemo::operator delete(void * ptr, size_t size) noexcept
{
	std::cout << "operator delete "<< size<< std::endl;
	return ::operator delete(ptr);
}

void MemoryDemo::operator delete[](void * ptr) noexcept
{
	std::cout << "operator delete[]" << std::endl;
	return ::operator delete(ptr);
}

void MemoryDemo::operator delete(void * ptr, const std::nothrow_t &nothrow) noexcept
{
	std::cout << "operator delete nothrow" << std::endl;
	return ::operator delete(ptr,nothrow);
}

void MemoryDemo::operator delete[](void * ptr, const std::nothrow_t &nothrow) noexcept
{
	std::cout << "operator delete[] nothrow" << std::endl;
	return ::operator delete[](ptr, nothrow);
}

