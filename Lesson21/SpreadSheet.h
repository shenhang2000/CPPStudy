#pragma once
#include <string>
class SpreadSheetCell;
class SpreadSheetApp;

struct Coord
{
	int x;
	int y;
	Coord(int x, int y)
	{
		this->x = x;
		this->y = y;
	}
};

class SpreadSheet
{
public:
	
    SpreadSheet(int nRow,int nColumn,SpreadSheetApp* app=nullptr);

	SpreadSheet(const SpreadSheet& other);

	SpreadSheet& operator=(const SpreadSheet& other);

	SpreadSheet(SpreadSheet&& other) noexcept;

	SpreadSheet& operator=(SpreadSheet&& other) noexcept;

public:
    void Save(const std::string& filePath);

    static void SetDefaultExtension(const std::string& strExt);

    static void DumpSpreadSheet(SpreadSheet& spreadSheet);

    void SetApp(SpreadSheetApp* pApp);

	~SpreadSheet();
private:
    SpreadSheet();
public:
	/*
	 * @return 返回true表示设置值成功，否则失败，比如索引超出表格的行数或者列数
	*/
	bool SetCell(int nRow, int nColumn, const std::string& mValue);

	bool GetCell(int nRow,int nColumn,std::string& strOutValue) const;

    SpreadSheetCell& GetCell(int nRow,int nColumn);
    const SpreadSheetCell& GetCell(int nRow,int nColumn) const;
	//返回一行单元格子,长度是mRow
	SpreadSheetCell& operator [](Coord c);
	//SpreadSheetCell& operator [](Coord c) const;
public:
	friend void swap(SpreadSheet& first, SpreadSheet& second) noexcept;

	void Init();
	void Fini();
	void InitFrom(const SpreadSheet& other);
	void MoveFrom(SpreadSheet& other);
private:
	SpreadSheetCell** mCells;
	int mRow;
	int mColumn;
	std::string mName;
    SpreadSheetApp* mApp;
public:
	const static int kDefaultRow = 100;
	const static int kDefaultColumn = 26;

	static std::string sDefaultExtension;

	static std::string sDefaultFileName;
};







