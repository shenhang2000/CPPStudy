#pragma once
#include "SpreadSheetCell.h"

class DoubleSpreadSheetCell :
	public SpreadSheetCell
{
public:
	DoubleSpreadSheetCell();
	DoubleSpreadSheetCell(double dValue);
	~DoubleSpreadSheetCell();

	//void SetValue(double dValue);
	//double GetValue();
	//SpreadSheetCell operator+ (const SpreadSheetCell& other);
	void SetValue(const std::string& strValue);
	void SetValue(double dValue);
	virtual std::string Trim(const std::string& strValue) override;

	DoubleSpreadSheetCell operator-() const;

	//ǰ��++
	DoubleSpreadSheetCell& operator++();
	//����++
	DoubleSpreadSheetCell operator++(int n);


	explicit operator double() const;

public:
	// ͨ�� SpreadSheetCell �̳�
	virtual std::string GetValue() const override;
	virtual std::string GetValue() override;

private:
	double mdValue;

};

class DoubleSpreadSheetCellComparator
{
public:
	int operator()(DoubleSpreadSheetCell& first, DoubleSpreadSheetCell& second)
	{
		double dfirst = std::stod(first.GetValue());
		double dsecond = std::stod(second.GetValue());
		double ddiff= (dfirst - dsecond);
		return ddiff;
	}
};

