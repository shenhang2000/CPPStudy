#pragma once

template<typename T>
class Pointer
{
public:
	Pointer(T* ptr);
	virtual ~Pointer();

	Pointer(const Pointer& src) = delete;
	Pointer& operator = (const Pointer& rhs) = delete;

	T& operator *();

	const T& operator *() const;

	T* operator ->();

	const T* operator ->() const;

	//operator void*() const;

	operator bool() const;

	operator int() const = delete;

	friend bool operator !=(const Pointer<T>& lhs, std::nullptr_t rhs);
private:
	T* mPtr = nullptr;
};

template<typename T>
Pointer<T>::Pointer(T * ptr)
	:mPtr(ptr)
{

}

template<typename T>
Pointer<T>::~Pointer()
{
	delete mPtr;
	mPtr = nullptr;
}

template<typename T>
T & Pointer<T>::operator*()
{
	// TODO: 在此处插入 return 语句
	return *mPtr;
}

template<typename T>
const T & Pointer<T>::operator*() const
{
	// TODO: 在此处插入 return 语句
	return *mPtr;
}

template<typename T>
inline T * Pointer<T>::operator->()
{
	return mPtr;
}

template<typename T>
inline const T * Pointer<T>::operator->() const
{
	return mPtr;
}

//template<typename T>
//inline Pointer<T>::operator void*() const
//{
//	return mPtr;
//}

template<typename T>
inline Pointer<T>::operator bool() const
{
	return mPtr != nullptr;
}

template<typename T>
bool operator!=(const Pointer<T>& lhs, std::nullptr_t rhs)
{
	return lhs.mPtr!=rhs;
}

