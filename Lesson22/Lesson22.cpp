﻿// Lesson22.cpp : 此文件包含 "main" 函数。程序执行将在此处开始并结束。
//

#include "pch.h"
#include <iostream>
#include <vector>
#include <string>
#include <limits>
#include <cassert>

class Elem
{
public:
	Elem(std::string name,std::string value,int id)
		:Name(std::move(name)),Value(std::move(value)),Id(id)
	{
		std::cout << "Elem Construct" << std::endl;
	}

	Elem(Elem&& other)
		:Name(std::move(other.Name)), Value(std::move(other.Value)), Id(other.Id)
	{
		std::cout << "Elem Moved" << std::endl;
	}

	Elem(const Elem& other)
	{
		std::cout << "Elem Copied" << std::endl;
	}
//private:
	bool operator == (const Elem& other2)
	{
		//std::cout << "Elem Copied" << std::endl;
		return false;
	}

public:
	std::string Name;
	std::string Value;
	int Id;
};


void print_vec(std::vector<int> vec)
{
	//for (size_t i = 0; i < vec.size(); i++)
	//{
	//	std::cout << vec[i] << " ";
	//}
	//for(auto iter=vec.begin();iter!=vec.end();++iter) //auto iter=std::begin(vec)
	//{
	//	std::cout << *iter << " ";
	//}
	for (auto& elem:vec)
	{
		std::cout << elem << " ";
	}
	std::cout << std::endl;
}

//数据标准化：最高100分，分数/最高分*100


int main()
{
	std::vector<int> IntVec;

	std::vector<std::string> StrVec;

	std::vector<int> IntVec2(100,3);

	std::vector<std::string> StrVec2(20,"hello world");

	auto& first = IntVec2[0];

	first = 208;

	IntVec2[0] = 99;

	std::cout << "first element="<<IntVec2.front()<<",last element="<<IntVec2.back() << std::endl;

	//std::vector<double> scores(10);
	//double curMaxScore = -std::numeric_limits<double>::infinity();//最小值，确保第一个元素开始循环时时最大值
	//for (size_t i = 0; i < scores.size(); i++)
	//{
	//	std::cout << "Enter score " << (i + 1) << ":";
	//	std::cin >> scores[i];
	//	if (scores[i] > curMaxScore)
	//	{
	//		curMaxScore = scores[i];
	//	}
	//}

	//for (auto& element:scores)
	//{
	//	double std_score = (element / curMaxScore) * 100.0;
	//	std::cout << std_score << " ";
	//}
	//std::cout << std::endl;


	std::vector<Elem> ElemVec;

	ElemVec.emplace_back("Start", "Beginer", 1);

	std::vector<int> InitVec({7,8,9,10,10,11,12});

	std::vector<int> InitVec2={ 1,2,3,4,5,6 };

	//auto vec_ptr = std::make_unique<std::vector<Elem> >(10);

	//ElemVec.reserve(4);

	//assert(ElemVec.size() == 2);

	//ElemVec.resize(4);

	//assert(ElemVec.size() == 4);

	//InitVec2.push_back(7);

	//Elem e;
	//ElemVec.push_back(e);

	//Elem e2;
	//ElemVec.push_back(e2);

	//ElemVec.assign(30,Elem());

	//std::vector<Elem> ElemVecEmpty;
	//ElemVec.swap(ElemVecEmpty);

	//ElemVec.clear();

	//if (ElemVecEmpty == ElemVec)
	//{

	//}

	//if (e == e2)
	//{

	//}

	//auto intIter = std::begin(InitVec);
	//for (; intIter!=std::end(InitVec);++intIter)
	//{
	//	std::cout << *intIter << std::endl;
	//	*intIter = *intIter + 1;
	//}

	//std::vector<Elem*> ElemPtrVec;
	//ElemPtrVec.push_back(new Elem());
	//

	//std::vector<std::reference_wrapper<std::string> > StrRefVec;

	//std::string str1 = "Hello";

	//StrRefVec.push_back(std::ref(str1));

	//StrRefVec.front().get() = "World";

	//std::cout << str1 << std::endl;

	////在尾部删除元素
	//StrRefVec.pop_back();

	print_vec(InitVec2);

	//InitVec2.insert(InitVec2.begin(), 9);
	//InitVec2.insert(InitVec2.begin() + 3, 4, 22);

	//InitVec2.insert(InitVec2.begin() + 5, {11,12,13,14});

	//InitVec2.insert(InitVec2.end(), InitVec.begin(), InitVec.end());

	//InitVec2.erase(InitVec2.begin());

	//InitVec2.erase(InitVec2.begin(),InitVec2.begin()+3);

	//print_vec(InitVec2);

	//std::vector<std::string> StrVecX;

	//std::string str = "Hello World";

	//StrVecX.push_back(str);

	//StrVecX.push_back(str);

	//StrVecX.push_back("Hi,World");

	//StrVecX.push_back(str);

	//StrVecX.push_back("Hello Nice");

	//StrVecX.push_back(std::move(str));

	print_vec(InitVec);

	for (auto i = InitVec.begin(); i != InitVec.end(); )
	{
		if (*i == 10)
		{
			i=InitVec.erase(i);
		}
		else
		{
			++i;
		}
	}

	print_vec(InitVec);

	return 0;
}

// 运行程序: Ctrl + F5 或调试 >“开始执行(不调试)”菜单
// 调试程序: F5 或调试 >“开始调试”菜单

// 入门提示: 
//   1. 使用解决方案资源管理器窗口添加/管理文件
//   2. 使用团队资源管理器窗口连接到源代码管理
//   3. 使用输出窗口查看生成输出和其他消息
//   4. 使用错误列表窗口查看错误
//   5. 转到“项目”>“添加新项”以创建新的代码文件，或转到“项目”>“添加现有项”以将现有代码文件添加到项目
//   6. 将来，若要再次打开此项目，请转到“文件”>“打开”>“项目”并选择 .sln 文件
