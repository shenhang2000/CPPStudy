字符串使用非常广泛
1.文本
2.数据，配置文件，csv文件,json文件,html

char 表示一个字符

error C4996: 'strcpy': This function or variable may be unsafe

strcpy
strlen
strcat

memset
memcpy

原始字符串字面量
R"(   xxxxx   )"

C++

string basic_string<char>

std::string str;
str.c_str();

C++17
to_chars

string_view
func(const std::string& strParam)

func(string_view sv)
{
    sv.find('a');
}

func("abc")

using namespace std；
using namespace std::string_view_literals;
auto mySV="my string_view"sv;

最大的问题：几乎每个项目都有一个自己的字符串类型