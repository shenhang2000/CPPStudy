#include <iostream>
#include <cstdarg>
using namespace std;

#define SQUARE(x) ((x)*(x))

#define DECLARE_CLASS(classname)  class classname{ \
    public: \
        int mem=1;\
}


DECLARE_CLASS(MyClass);

void test()
{
    std::cout<<"test1"<<std::endl;
}

void test(int)
{
    std::cout<<"test2"<<std::endl;
}

void func2019()
{
    std::cout<<"func2019"<<std::endl;
}

[[nodiscard]]int demo()
{
    return 2019;
}

void demo2(int param1,[[maybe_unused]]int param2)
{
    std::cout<<"param1="<<param1<<std::endl;
}

void debugOut(const char* str,...)
{
    va_list ap;
    va_start(ap,str);

    int arg1 = va_arg(ap,int);
    double arg2 = va_arg(ap,double);
    //char buf[1024];
    //vsprintf(buf,str,ap);
    va_end(ap);
}



int main()
{
    MyClass myobj;
    myobj.mem=2012;
    std::cout<<SQUARE(demo())<<std::endl;//2+3*2+3 //demo()*demo()

    std::string str1="Hello World";
    auto myStr="abcd"s;

    auto myStrView="Hello,2019"sv;

    std::cout<<myStrView<<std::endl;
    int switch_on = 4;

    switch (switch_on)
    {
    case 1:
        func2019();
        [[fallthrough]];
    case 2:
    {
        break;
    }
    case 3:
        test();
        [[fallthrough]];
    case 4:
        test(2);
    }
    demo();
    return 0;
}
